// import 'package:artokuspikeapp/screens/login.dart';
// import 'package:artokuspikeapp/screens/signup.dart';
// import 'package:flutter/material.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'RoutingNavigating',
//       home: LoginScreen(),
//       routes: {
//         LoginScreen.id: (context) => LoginScreen(),
//         SignupScreen.id: (context) => SignupScreen(),
//       }

//     );
//   }
// }

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:artokuspikeapp/app.dart';
import 'package:artokuspikeapp/repositories/repositories.dart';
import 'package:http/http.dart' as http;

void main() {
  final WeatherRepository weatherRepository = WeatherRepository(
      weatherApiClient: WeatherApiClient(httpClient: http.Client()));

  BlocSupervisor().delegate = SimpleBlocDelegate();
  runApp(App(weatherRepository: weatherRepository));
}
